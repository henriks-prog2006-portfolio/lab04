module Lib
    ( decodeMessage
    ) where


-- Function to split a string into a list of integers
splitToInts :: String -> [Int]
splitToInts = map read . words
{- words takes a string and splits it into a list of words,
   map applies the read function to each element on the list
   read converts a string to specified data type (int)
   "." composes to functions, the output of one function is the others input
-}

--Function to check if the min and max is unique
hasUniqueMinMax :: Ord a => [a] -> Bool
hasUniqueMinMax [] = False  -- to catch errors in empty lists
hasUniqueMinMax xs = unique (minimum xs) (maximum xs) xs
  where
    unique minElem maxElem xs = countOccurrences minElem xs == 1 && countOccurrences maxElem xs == 1
{-using the "Ord" class for min and max
create a helper function "unique" which takes 3 arguments
checks if the min and max both only exist once
-}

-- Function to check if the sum of min and max is even
isSumEven :: Integral a => [a] -> Bool
isSumEven xs = (sum [minimum xs, maximum xs]) `mod` 2 == 0
{- using integral to make 'mod' work, does not like "ord"
-}

-- Function to count occurrences of a value in a list
countOccurrences :: Eq a => a -> [a] -> Int
countOccurrences _ [] = 0   -- to catch empty lists, ignoring x
countOccurrences x (y:ys)
    | x == y = 1 + countOccurrences x ys
    | otherwise = countOccurrences x ys
{-if the list is empty returns 0
checks if the head of the list "y" is equal to x, and recursively calls
itself on the tail of the list, incrementing if it is
-}

-- | Decode an intergalactic message from a string.
-- The message is a sequence of integers separated by spaces.
--
-- >>> decodeMessage "5 5 5 8 1 2 3 4 9 8 2 3 4 1"
-- Nothing
--
-- >>> decodeMessage "5 5 5 8 1 2 3 4 9 8 2 3 4 9"
-- Nothing
--
-- >>> decodeMessage "5 5 5 8 1 2 3 4 9 8 2 3 4 8"
-- Just 3
-- 
-- >>> decodeMessage "5 5 5 1 2 3 4 8 2 3"
-- Nothing
-- Function to parse input, validate, and compute the result
decodeMessage :: String -> Maybe Int
decodeMessage msg =
    case splitToInts msg of
        [] -> Nothing -- to catch empty lists
        numbers ->
            let minNum = minimum numbers
                maxNum = maximum numbers
                sumMinMax = minNum + maxNum
                target = sumMinMax `div` 2
                occurrences = countOccurrences target numbers
                unique = hasUniqueMinMax numbers
                evenSum = isSumEven numbers
            in
                if unique && evenSum
                    then Just occurrences
                    else Nothing
{-
let, sets up variables
case of, for pattern matching
in, separates variables from if statement
Just, wraps a variable into a maybe type
-}
