# Lab4 - Space task (Haskell)


## Learning objectives for Lab4, as outlined by lecturer

> - Basics of error handling with Maybe and Either types.



----------------



# The task: Galactic Greetings

The task is to create a modular program that takes a string message and attempts to decode it according to the rules provided:

> - Unique Min-Max Club: the minimum and maximum numbers in the string must be unique.
> - Even Sum Extravaganza: The sum of the minimum and maximum numbers must be an even number
> - Counting Cosmic Vibes: Count the number of times the minimum plus maximum, divided by 2, appears in the string. That magical number is our extraterrestrial message.

#### Example Input:
```
5 5 5 8 1 2 3 4 9 8 2 3 4
```
#### Output:
```
3
```
--------------
### Functions implemented:

#### splitToInts :: String -> [Int]
Function to split a string into a list of integers
#### hasUniqueMinMax :: Ord a => [a] -> Bool
Function to check if the min and max is unique
#### isSumEven :: Integral a => [a] -> Bool
Function to check if the sum of min and max is even
#### countOccurrences :: Eq a => a -> [a] -> Int
Function to count occurrences of a value in a list
#### decodeMessage :: String -> Maybe Int
Function to parse input, validate, and compute the result
